<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Form\TodoListType;
use App\Entity\TodoList;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class HomeController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function index(Request $request, SessionInterface $session)
    {
        
        dump($session->get('tache'));
        $todoList = new TodoList();
        

        $form = $this->createForm(TodoListType::class, $todoList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $em->persist($todoList);
            
            
            $em->flush();
        }

        return $this->render('home/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/rm/{list}", name="remove_list")
     */
    public function removeList(TodoList $list) { 
        
        if(!is_null($list)) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($list);
            $em->flush();
        }
        return $this->redirectToRoute('home');
    }
}
