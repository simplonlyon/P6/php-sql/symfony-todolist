<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use App\Entity\Task;

class SessionExampleController extends Controller
{
    /**
     * @Route("/session/example", name="session_example")
     */
    public function index(SessionInterface $session)
    {
        /**
         * Une session permet d'accéder à des informations entre 
         * plusieurs pages pour un.e même utilisateur.ice (c'est comme le localStorage du js plus ou moins)
         * On stock des choses dans la session avec le SessionInterface
         * de symfony et en utilisant la méthode set qui attend en premier
         * argument une clef et en deuxième argument une valeur pour cette clef
         * On pourra accéder aux valeurs de la session avec la méthode get
         * qui attend en argument la clef qu'on veut accéder
         */
        $tache = new Task();
        $tache->setDescription("bloup");
        $tache->getDone(true);
        $session->set("tache", $tache);
        return $this->render('session_example/index.html.twig', [
            'controller_name' => 'SessionExampleController',
        ]);
    }
}
