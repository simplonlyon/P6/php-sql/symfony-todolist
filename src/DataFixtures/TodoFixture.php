<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\TodoList;
use App\Entity\Task;
/**
 * Classe qui permet de charger un set de données bidons dans la
 * base de données (afin de pouvoir tester le site rapidement et que
 * tous les membres du projet aient les même données)
 * On peut générer la classe avec php bin/console make:fixture
 * On l'exécute ensuite avec php bin/console doctrine:fixtures:load
 */
class TodoFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 3; $i++) { 
            
            $list = new TodoList();
            $list->setName("Name $i");

            for ($y=0; $y < 4; $y++) { 
                $task = new Task();
                $task->setDescription("Task $y");
                $task->setDone(false);
                $list->addTask($task);
            }
            $manager->persist($list);
        }

        $manager->flush();
    }
}
